#!/bin/bash
BASEDIR=$(dirname "$(readlink -f "$0")")
if [[ ! $PATH == *"${BASEDIR}/bin"* ]]; then
  export PATH="${BASEDIR}/bin:${PATH}"
fi

export PROJECT_OWNER='replace_with_gitlab_owner'
export CI_PROJECT_ID='replace_with_gitlab_iac_project_id'
export GITLAB_USER_LOGIN=$PROJECT_OWNER
export GITLAB_USER_TOKEN='replace_with_gitlab_token'

export EXAMPLE_ENV_VARIABLE='example'

export SECRETS_GITHUB_PAT='replace_with_github_pat'
