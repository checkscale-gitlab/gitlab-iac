package responses

type Jobs []struct {
	Pipeline struct {
		SHA string `json:"sha"`
	} `json:"pipeline"`
	Stage string `json:"stage"`
}
