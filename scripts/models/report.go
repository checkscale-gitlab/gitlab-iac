package models

type Report struct {
	Create int `json:"create"`
	Update int `json:"update"`
	Delete int `json:"delete"`
}
