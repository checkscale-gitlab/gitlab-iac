package utils

import (
	"bytes"
	"errors"
	"os/exec"
)

func Cmd(dir string, cmd ...string) (string, error) {
	run := exec.Command(cmd[0], cmd[1:]...) //nolint:gosec
	run.Dir = dir
	var stderr bytes.Buffer
	run.Stderr = &stderr
	stdout, err := run.Output()
	if err != nil {
		return "", errors.New(stderr.String())
	}
	return string(stdout), nil
}
