package utils

import (
	"encoding/json"
	"gitlab-iac/scripts/models"
)

func StringToState(str string) (models.State, error) {
	state := models.State{}
	err := json.Unmarshal([]byte(str), &state)
	if err != nil {
		return state, err
	}
	return state, nil
}
