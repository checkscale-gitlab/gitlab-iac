package utils

import (
	"io/ioutil"
	"strings"
)

func GetProjectFolders() []string {
	var out []string
	all, _ := ioutil.ReadDir(GetRepoDir())
	for _, file := range all {
		if file.IsDir() && !strings.HasPrefix(file.Name(), ".") {
			out = append(out, file.Name())
		}
	}
	return out
}
