package utils

import (
	"log"
	"path"
	"path/filepath"
	"runtime"
)

func GetBaseDir() string {
	_, scripts, _, _ := runtime.Caller(0)
	abs, err := filepath.Abs(path.Dir(scripts) + "/../../") // utils -> scripts -> parent
	if err != nil {
		log.Fatalln(err)
	}
	return abs + "/"
}
func GetRepoDir() string {
	return GetBaseDir() + "repos/"
}
